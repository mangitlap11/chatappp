import 'package:chatapp/helper/helperfunction.dart';
import 'package:chatapp/service/auth.dart';
import 'package:chatapp/service/database.dart';
import 'package:chatapp/widget/widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'chat_room_screen.dart';

class SignIn extends StatefulWidget {
  final Function toggle;
  SignIn(this.toggle);

  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  AuthMethod authMethod = AuthMethod();
  DatabaseMethod databaseMethod = DatabaseMethod();
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  bool isLoading = false;
  QuerySnapshot snapshotUserInfo;
  signIn() {
    if (formKey.currentState.validate()) {
      HelperFunctions.saveUserEmailSharedPrefernce(
          emailTextEditingController.text);

      //TODO functions to get user detail
      setState(() {
        isLoading = true;
      });
      databaseMethod
          .getUserByUserEmail(emailTextEditingController.text)
          .then((value) {
        snapshotUserInfo = value;
        HelperFunctions.saveUserNameSharedPrefernce(
            snapshotUserInfo.docs[0]["name"]);
      });

      authMethod
          .signInWithEmailAndPassword(emailTextEditingController.text,
              passwordTextEditingController.text)
          .then((value) {
        if (value != null) {
          HelperFunctions.saveuserLoggedInSharedPreference(true);
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => ChatRoom()));
        }
      });
    }
  }

  @override
  void initState() {
    // databaseMethod
    //     .getUserByUserEmail(emailTextEditingController.text)
    //     .then((value) {
    //   snapshotUserInfo = value;
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBarMain(context),
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height - 50,
            alignment: Alignment.bottomCenter,
            child: Container(
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Form(
                    key: formKey,
                    child: Column(
                      children: [email(), password()],
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  fogetPassword(),
                  SizedBox(
                    height: 16,
                  ),
                  signInbtn(),
                  SizedBox(
                    height: 16,
                  ),
                  signInWithGooglebtn(),
                  SizedBox(
                    height: 16,
                  ),
                  register(),
                  SizedBox(
                    height: 50,
                  )
                ],
              ),
            ),
          ),
        ));
  }

  Widget email() {
    return TextFormField(
      validator: (value) {
        return RegExp(
                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+[a-zA-Z0-9]+\.[a-zA-Z]+")
                .hasMatch(value)
            ? null
            : "Please provide a valid email";
      },
      controller: emailTextEditingController,
      onEditingComplete: () {
        FocusScope.of(context).nextFocus();
      },
      decoration: textFieldInputDecoration("email"),
      style: simpleTextStyle(),
    );
  }

  Widget password() {
    return TextFormField(
      obscureText: true,
      validator: (value) {
        return value.length > 6 ? null : "Please provide password 6+ character";
      },
      controller: passwordTextEditingController,
      onEditingComplete: () {
        signIn();
      },
      decoration: textFieldInputDecoration("password"),
      style: simpleTextStyle(),
    );
  }

  Widget fogetPassword() {
    return Container(
      alignment: Alignment.centerRight,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Text("Forgot Password?", style: simpleTextStyle()),
      ),
    );
  }

  Widget signInbtn() {
    return GestureDetector(
      onTap: () {
        signIn();
      },
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Text(
          "Sign In",
          style: mediumTextStyle(),
        ),
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xff007EF4),
              const Color(0xff2A75BC),
            ]),
            borderRadius: BorderRadius.circular(30)),
      ),
    );
  }

  Widget signInWithGooglebtn() {
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Text(
        "Sign In with Google",
        style: TextStyle(color: Colors.black87, fontSize: 17),
      ),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(30)),
    );
  }

  Widget register() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Don't have account?",
          style: mediumTextStyle(),
        ),
        SizedBox(
          width: 8,
        ),
        GestureDetector(
          onTap: () {
            widget.toggle();
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Text(
              "Register now",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 17,
                  decoration: TextDecoration.underline),
            ),
          ),
        )
      ],
    );
  }
}
