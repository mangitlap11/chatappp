import 'package:chatapp/helper/helperfunction.dart';
import 'package:chatapp/service/auth.dart';
import 'package:chatapp/widget/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:chatapp/service/database.dart';
import 'chat_room_screen.dart';

class SignUp extends StatefulWidget {
  final Function togggle;
  SignUp(this.togggle);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  // HelperFunctions helperFunctions = HelperFunctions();
  AuthMethod authMethod = AuthMethod();
  final formKey = GlobalKey<FormState>();
  bool isLoading = false;
  DatabaseMethod databaseMethod = DatabaseMethod();
  TextEditingController userNameTextEditingController = TextEditingController();
  TextEditingController emailTextEditingController = TextEditingController();
  TextEditingController passwordTextEditingController = TextEditingController();
  signMeUp() {
    if (formKey.currentState.validate()) {
      Map<String, String> userInfoMap = {
        "name": userNameTextEditingController.text,
        "email": emailTextEditingController.text
      };
      HelperFunctions.saveUserEmailSharedPrefernce(
          emailTextEditingController.text);
      HelperFunctions.saveUserNameSharedPrefernce(
          userNameTextEditingController.text);
      setState(() {
        isLoading = true;
      });
      authMethod
          .signUpWithEmailAndPassword(emailTextEditingController.text,
              passwordTextEditingController.text)
          .then((value) {
        databaseMethod.upLoadUserInfo(userInfoMap);
        HelperFunctions.saveuserLoggedInSharedPreference(true);
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => ChatRoom()));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: appBarMain(context),
        body: isLoading
            ? Container(
                child: Center(child: CircularProgressIndicator()),
              )
            : SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height - 50,
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    alignment: Alignment.bottomCenter,
                    padding: EdgeInsets.symmetric(horizontal: 24),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Form(
                          key: formKey,
                          child: Column(
                            children: [
                              user(),
                              email(),
                              password(),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        forgotPassword(),
                        SizedBox(
                          height: 16,
                        ),
                        signUpbtn(),
                        SizedBox(
                          height: 16,
                        ),
                        signInWithGooglebtn(),
                        SizedBox(
                          height: 16,
                        ),
                        signInbtn(),
                        SizedBox(
                          height: 50,
                        )
                      ],
                    ),
                  ),
                ),
              ));
  }

  Widget user() {
    return TextFormField(
      onEditingComplete: () {
        FocusScope.of(context).nextFocus();
      },
      validator: (value) {
        return value.isEmpty || value.length < 2
            ? "Please provide a valid UserName"
            : null;
      },
      controller: userNameTextEditingController,
      decoration: textFieldInputDecoration("user name"),
      style: simpleTextStyle(),
    );
  }

  Widget email() {
    return TextFormField(
      onEditingComplete: () {
        FocusScope.of(context).nextFocus();
      },
      validator: (value) {
        return RegExp(
                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+[a-zA-Z0-9]+\.[a-zA-Z]+")
                .hasMatch(value)
            ? null
            : "Please provide a valid email";
      },
      controller: emailTextEditingController,
      decoration: textFieldInputDecoration("email"),
      style: simpleTextStyle(),
    );
  }

  Widget password() {
    return TextFormField(
      onEditingComplete: () {
        signMeUp();
      },
      obscureText: true,
      validator: (value) {
        return value.length > 6 ? null : "Please provide password 6+ character";
      },
      controller: passwordTextEditingController,
      decoration: textFieldInputDecoration("password"),
      style: simpleTextStyle(),
    );
  }

  Widget forgotPassword() {
    return Container(
      alignment: Alignment.centerRight,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Text("Forgot Password?", style: simpleTextStyle()),
      ),
    );
  }

  Widget signUpbtn() {
    return GestureDetector(
      onTap: () {
        signMeUp();
      },
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 20),
        child: Text(
          "Sign Up",
          style: mediumTextStyle(),
        ),
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              const Color(0xff007EF4),
              const Color(0xff2A75BC),
            ]),
            borderRadius: BorderRadius.circular(30)),
      ),
    );
  }

  Widget signInWithGooglebtn() {
    return Container(
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Text(
        "Sign In with Google",
        style: TextStyle(color: Colors.black87, fontSize: 17),
      ),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(30)),
    );
  }

  Widget signInbtn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Already have account?",
          style: mediumTextStyle(),
        ),
        SizedBox(
          width: 8,
        ),
        GestureDetector(
          onTap: () {
            widget.togggle();
          },
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 8),
            child: Text(
              "SignIn now",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 17,
                  decoration: TextDecoration.underline),
            ),
          ),
        )
      ],
    );
  }
}
