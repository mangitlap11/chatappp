import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseMethod {
  Future<QuerySnapshot> getUserByUserName(String username) async {
    QuerySnapshot result = await FirebaseFirestore.instance
        .collection("users")
        .where("name", isEqualTo: "$username")
        .get();
    return result;
  }

  Future<QuerySnapshot> getUserByUserEmail(String userEmail) async {
    QuerySnapshot result = await FirebaseFirestore.instance
        .collection("users")
        .where("email", isEqualTo: "$userEmail")
        .get();
    return result;
  }

  upLoadUserInfo(userMap) {
    FirebaseFirestore.instance.collection("users").add(userMap);
  }

  createChatRoom(String chatRoomId, chatRoomMap) {
    FirebaseFirestore.instance
        .collection("ChatRoom")
        .doc(chatRoomId)
        .set(chatRoomMap)
        .catchError((e) {
      print(e.toString());
    });
  }
}
