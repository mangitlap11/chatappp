// import 'package:chatapp/helper/constants.dart';
// import 'package:chatapp/service/database.dart';
// import 'package:chatapp/view/conversation_screen.dart';
// import 'package:chatapp/view/search.dart';
// import 'package:chatapp/widget/widget.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';

// DatabaseMethod databaseMethod = DatabaseMethod();
// TextEditingController searchTextEdittingController = TextEditingController();
// QuerySnapshot searchSnapshot;
// Widget searchTile({String userName, String userEmail}) {
//   return Container(
//     child: Row(
//       children: [
//         Column(
//           children: [
//             Text(
//               userName,
//               style: simpleTextStyle(),
//             ),
//             Text(
//               userEmail,
//               style: simpleTextStyle(),
//             ),
//           ],
//         ),
//         Spacer(),
//         GestureDetector(
//           onTap: () {
//             createChatRoomAndStarConversation(username: userName);
//           },
//           child: Container(
//             decoration: BoxDecoration(
//                 color: Colors.blue, borderRadius: BorderRadius.circular(30)),
//             child: Text("Message"),
//             padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
//           ),
//         )
//       ],
//     ),
//   );
// }

// Widget searchList() {
//   return searchSnapshot != null
//       ? ListView.builder(
//           shrinkWrap: true,
//           itemCount: searchSnapshot.docs.length,
//           itemBuilder: (context, index) {
//             return searchTile(
//               userName: searchSnapshot.docs[index]["name"],
//               userEmail: searchSnapshot.docs[index]["email"],
//             );
//           })
//       : Container(
//           child: Center(
//             child: CircularProgressIndicator(),
//           ),
//         );
// }

// createChatRoomAndStarConversation({String username}) {
//   if (username != Constants.myName) {
//     List<String> users = [username, Constants.myName];
//     String chatRoomId = getChatRoomId(username, Constants.myName);
//     Map<String, dynamic> chatRoomMap = {
//       "users": users,
//       "chatroomId": chatRoomId
//     };
//     DatabaseMethod().createChatRoom(chatRoomId, chatRoomMap);
//     Navigator.push(
//         context, MaterialPageRoute(builder: (context) => ConversationScreen()));
//   } else {
//     print("you can't send message to myself");
//   }
// }

// initiateSearch() async {
//   await databaseMethod
//       .getUserByUserName(searchTextEdittingController.text)
//       .then((value) {
//     setState(() {
//       searchSnapshot = value;
//     });
//   });
// }
